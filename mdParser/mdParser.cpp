/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/27/2019
 * License: MIT
 **/
#include "mdParser.h"

namespace MUParser {
MDParser::MDParser(
  const std::unordered_map<std::string, std::string> &ld,
  const std::unordered_map<std::string, std::string> &rd
) {
  leftDelimeters = ld;
  rightDelimeters = rd;
}

MDParser::~MDParser() {}

StringToken MDParser::Tokenize(std::string s) {
  std::string leftDelimeter, rightDelimeter, text;
  bool endLeftDelimeter, endRightDelimeter = false;
  const size_t s_size = s.size();
  size_t endLD, endRD = 0;
  size_t i = 0;

  // Get left delimeter
  do {
    leftDelimeter += s[i];
    endLeftDelimeter = (leftDelimeters.find(leftDelimeter) == leftDelimeters.end());
    ++i;
  } while (!endLeftDelimeter && i < s_size);
  endLD = i - 1;

  // Get right delimeter
  i = s_size - 1;
  do {
    rightDelimeter = s[i] + rightDelimeter;
    endRightDelimeter = (rightDelimeters.find(rightDelimeter) == rightDelimeters.end());
    --i;
  } while (!endLeftDelimeter && i > 0);
  endRD = i + 1;

  // Creating token
  StringToken *token = new StringToken();
  token->text = s.substr(endLD, endRD - endLD);
  token->rightDelimeter = s.substr(endRD);
  token->leftDelimeter = s.substr(0, endLD);

  return *token;
}

StringToken MDParser::Parse(StringToken &t) {
  StringToken *token = new StringToken();
  token->leftDelimeter = leftDelimeters[t.leftDelimeter];
  token->rightDelimeter = leftDelimeters[t.rightDelimeter];
  token->text = t.text;
  return *token;
}
};