/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/27/2019
 * License: MIT
 **/
#ifndef MD_PARSER_H
#define MD_PARSER_H

#include <iostream>
#include <unordered_map>
#include "../lib/parser.h"
#include "../lib/token.h"

namespace MUParser {
typedef Token<std::string> StringToken;

class MDParser : Parser<std::string> {
 public:
  // Constructor with left and right delimeters
  MDParser(const std::unordered_map<std::string, std::string> &ld,
           const std::unordered_map<std::string, std::string> &rd);

  // Deconstructor
  ~MDParser();

  // Create a token from the input
  StringToken Tokenize(std::string s);

  // Parse token
  StringToken Parse(StringToken &t);
};
}  // namespace MUParser

#endif