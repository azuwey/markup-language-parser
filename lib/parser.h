/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/27/2019
 * License: MIT
 **/
#ifndef PARSER_H
#define PARSER_H

#include <unordered_map>
#include "token.h"

namespace MUParser {
template <typename T>
struct Parser {
 public:
  std::unordered_map<T, T> leftDelimeters;
  std::unordered_map<T, T> rightDelimeters;
  Token<T> Tokeinze(T);
  Token<T> Parse(Token<T>);
};
}  // namespace MUParser

#endif