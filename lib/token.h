/**
 * Author: Zarandi David (Azuwey)
 * Date: 02/27/2019
 * License: MIT
 **/
#ifndef TOKEN_H
#define TOKEN_H

namespace MUParser {
template <typename T>
struct Token {
 public:
  T text, leftDelimeter, rightDelimeter;
};
}  // namespace MUParser

#endif