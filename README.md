# Markup Language Parser

## What is the goal of this project

The goal of this project is to build a configurable markup language parser which can be used in different platforms (eg.: Web with WebAssembly, in native applications and so on) and with different markup languages.

## Why C++

Because one of the goal is to able to use with different platforms, and C++ is fast enough without any compromises.

## License

This project is under MIT license